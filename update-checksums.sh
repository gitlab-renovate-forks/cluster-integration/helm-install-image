#!/usr/bin/env bash

set -ex

KUBECTL_VERSION=$(sed -nr "s/.*KUBECTL_VERSION: '(.*)'/\\1/p" .gitlab-ci.yml)
HELM3_VERSION=$(sed -nr "s/.*HELM3_VERSION: '(.*)'/\\1/p" .gitlab-ci.yml)

for TARGETARCH in amd64 arm64; do
  # kubectl
  kubectl_checksum=$(curl -fLSs "https://dl.k8s.io/v${KUBECTL_VERSION}/kubernetes-client-linux-${TARGETARCH}.tar.gz.sha512")
  echo "${kubectl_checksum}  kubectl.tar.gz" >"kubectl.sha512sum.${TARGETARCH}"

  # helm
  curl -fLSs "https://get.helm.sh/helm-v${HELM3_VERSION}-linux-${TARGETARCH}.tar.gz.sha256sum" -o "helm3.sha256sum.${TARGETARCH}"
done
